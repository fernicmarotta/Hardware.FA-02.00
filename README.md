# Hardware.FA-02.00

## Fuente de Alimentación

Autor: 

- Agustín Gonzalez

Revisión: 

- German Vazquez

Hardware:
  
  - Fuente de 48V a 15V Regulable (Fuente Switching: LM2576).
  - Cargador Serie de Tensión Constante para Batería de Ácido Plomo (BQ24450)
  - Fuente de VBateria a 3,4V para LEDs de Potencia.
  - Fuente de VBateria a 2,5V para LEDs de Potencia.
  - Fuente de VBateria a 12V Regulable (Fuente Switching: LM2576) para placas varias.
  -  Fuente de VBateria a 5V Fija (Fuente Switching: LM2576) para placas varias

Software:

- Eagle 6.5.0 Light

Usos:

- Semáforo
- Consola
- Comandos